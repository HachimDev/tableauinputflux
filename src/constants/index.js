export default {
    ADD_NEW_USER: 'ADD_NEW_USER',
    DELETE_USER: 'DELETE_USER',
    UPDATE_USER: 'UPDATE_USER',
    EDIT_USER : 'EDIT_USER',
    SIGN_IN: 'SIGN_IN',

    ADD_NEW_GROUP:'ADD_NEW_GROUP',
    EDIT_GROUP: 'EDIT_GROUP'
}