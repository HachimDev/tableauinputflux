import React, { Component } from "react";
import "./AuthForm.css";
import UserActions from '../../actions/UserActions';
import UserStore from '../../stores/UserStore';

class AuthForm extends Component {

  constructor() {
      super();
    this.state = {
      user: {
        email: "",
        pwd: ""
      },
      isValidUser: false
    };

    this._onChange = this._onChange.bind(this);
  }

  _onChange() {
    this.setState({ isValidUser: UserStore.isLogged()});
  }

  componentDidMount() {
    UserStore.addChangeListener(this._onChange);
  }

  componentWillUpdate(nextProps, nextState){
    console.log("[ComponentWillMount] isValidUser : ", nextState.isValidUser);
    
        if(nextState.isValidUser){
          this.props.history.push('/tableau')
        }
  }

  componentWillUnmount() {
    UserStore.removeChangeListener(this._onChange);
  }

  _getFreshInput(){
      return {
          email:'',
          pwd:''
      }
  }

  _updateEmailState = (event) => {
    let user = {
      email: event.target.value,
      pwd: this.state.user.pwd
    };

    this.setState({user:user});
  };

  _updatePwdState = (event) => {
      let user = {
          email: this.state.user.email,
          pwd:event.target.value
      };

      this.setState({user:user});
  }

  connectUser = (event) => {
    event.preventDefault();
    UserActions.signIn(this.state.user);
    this.setState({
        user: this._getFreshInput()
    });
  };

  render() {
    return (
      <div className="authForm">
        <form onSubmit={this.connectUser.bind(this)}>
          <div className="form-group">
            <label htmlFor="inputEmail">Email address</label>
            <input
              type="email"
              className="form-control"
              id="inputEmail"
              placeholder="Enter email"
              value={this.state.user.email}
              onChange={this._updateEmailState.bind(this)}
            />
            <small id="emailHelp" className="form-text text-muted">
              some small text to add later
            </small>
          </div>
          <div className="form-group">
            <label htmlFor="inputPassword">Password</label>
            <input
              type="password"
              className="form-control"
              id="inputPassword"
              placeholder="Password"
              value={this.state.user.pwd}
              onChange={this._updatePwdState.bind(this)}
            />
          </div>
          <button type="submit" className="btn btn-primary">
            Sign In
          </button>
        </form>
      </div>
    );
  }
}

export default AuthForm;
