import React, { Component } from "react";
import LigneTab from "../LigneTab/LigneTab";
import UserActions from "../../actions/UserActions";

import './Tableau.css';

class Tableau extends Component {
  deleteUser = id => {
    if (
      window.confirm(
        "Do you really want to delete the user " +
          this.props.users[id].name +
          " ?"
      )
    ) {
      UserActions.deleteUser(id);
    }
  };

  editUser = (user) => {
    UserActions.editUser(user);
    
    this.props.history.push('/new-user');
  };

  getAllUsersRows = () => {
    let rows = [];

    this.props.users.map((user, index) => {
      rows.push(
        <LigneTab
          key={index}
          id={user.id}
          name={user.name}
          email={user.email}
          phone={user.phone}
          deletebtn={this.deleteUser.bind(this, index)}
          updatebtn={this.editUser.bind(this, user)}
        />
      );
    });
    return rows;
  };

  render() {
    return (

      <table className="table table-striped table-dark">
        <thead className="">
          <tr>
            <th>#</th>
            <th>User Name</th>
            <th>User Email</th>
            <th>User Phone</th>
            <th>Remove</th>
            <th>Update</th>
          </tr>
        </thead>
        <tbody>{this.getAllUsersRows()}</tbody>
      </table>
    );
  }
}

export default Tableau;
