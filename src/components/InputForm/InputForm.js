import React, { Component } from "react";
import UserActions from "../../actions/UserActions";

import "./InputForm.css";

class inputForm extends Component {
  constructor(props) {
    super(props);

    this._getFreshUser = this._getFreshUser.bind(this);

    this.state = {
      user: this._getFreshUser(),
      userToEdit: this.props.userToEdit,
      wantToEdit: this.props.wantToEdit,
      errorMsgName: "",
      errorMsgEmail: "",
      errorMsgPhone: ""
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps !== this.props) {
      this.setState({
        userToEdit: nextProps.userToEdit,
        wantToEdit: nextProps.wantToEdit
      });
    }
  }

  _getFreshUser() {
    return {
      name: "",
      email: "",
      phone: "",
      pwd:"123456"
    };
  }

  _addNewUser(event) {
    event.preventDefault();
    this.setState({
      errorMsgName: "",
      errorMsgEmail: "",
      errorMsgPhone: ""
    });
    let temoin = 3; //magic var...

    //condition de validation
    if (!this.state.user.name) {
      this.setState({ errorMsgName: "Invalid Name Input !" });
      temoin--;
    }
    if (!this.state.user.email) {
      this.setState({ errorMsgEmail: "Invalid Email Input !" });
      temoin--;
    }
    if (!this.state.user.phone) {
      this.setState({ errorMsgPhone: "Invalid Phone Input !" });
      temoin--;
    }
    if (temoin === 3) {
      //Reset mesg Errors if everything is correct

      this.setState({
        errorMsgName: "",
        errorMsgEmail: "",
        errorMsgPhone: ""
      });

      let formUser = {
        name: this.state.user.name,
        email: this.state.user.email,
        phone: this.state.user.phone
      };
      UserActions.addNewUser(formUser);
      this.setState({ user: this._getFreshUser() });
      this.props.history.push('/tableau');
    }
  }

  _updateExistingUser(event) {
    event.preventDefault();
    let id = this.state.userToEdit.id;
    let updatedUser = {
      id: id,
      name: this.state.userToEdit.name,
      email: this.state.userToEdit.email,
      phone: this.state.userToEdit.phone
    };

    this.setState({ wantToEdit: false });
    UserActions.updateUser(updatedUser);
    this.setState({ userToEdit: this._getFreshUser() });
    console.log(this.state.wantToEdit);
  }

  _updateNameState(event) {
    if (!this.state.wantToEdit) {
      let user = {
        name: event.target.value,
        email: this.state.user.email,
        phone: this.state.user.phone
      };
      this.setState({ user: user });
    } else {
      let user = {
        name: event.target.value,
        email: this.state.userToEdit.email,
        phone: this.state.userToEdit.phone,
        id: this.state.userToEdit.id
      };
      this.setState({ userToEdit: user });
    }
  }

  _updateEmailState(event) {
    if (!this.state.wantToEdit) {
      let user = {
        name: this.state.user.name,
        email: event.target.value,
        phone: this.state.user.phone
      };
      this.setState({ user: user });
    } else {
      let user = {
        name: this.state.userToEdit.name,
        email: event.target.value,
        phone: this.state.userToEdit.phone,
        id: this.state.userToEdit.id
      };
      this.setState({ userToEdit: user });
    }
  }

  _updatePhoneState(event) {
    if (!this.state.wantToEdit) {
      let user = {
        name: this.state.user.name,
        email: this.state.user.email,
        phone: event.target.value
      };
      this.setState({ user: user });
    } else {
      let user = {
        name: this.state.userToEdit.name,
        email: this.state.userToEdit.email,
        phone: event.target.value,
        id: this.state.userToEdit.id
      };
      this.setState({ userToEdit: user });
    }
  }

  render() {

    //Dynamic form depending if we want to edit
    let nameValue = "";
    let emailValue = "";
    let phoneValue = "";
    let submitForm = "";
    let btnValue = "";

    if (!this.state.wantToEdit) {
      submitForm = this._addNewUser.bind(this);
      nameValue = this.state.user.name;
      emailValue = this.state.user.email;
      phoneValue = this.state.user.phone;
      btnValue = "ADD USER";
    } else {
      submitForm = this._updateExistingUser.bind(this);
      nameValue = this.state.userToEdit.name;
      emailValue = this.state.userToEdit.email;
      phoneValue = this.state.userToEdit.phone;
      btnValue = "UPDATE";
    }

    return (
      <div className="inputForm">
        <form onSubmit={submitForm} >
          <div className="form-group">
          <label htmlFor="inputName" >Name</label>
            <input
              type="text"
              placeholder="Name"
              name="name"
              id="inputName"
              className="form-control"
              value={nameValue}
              onChange={this._updateNameState.bind(this)}
            />
          <small id="nameHelp" className="form-text text-muted">
          {this.state.errorMsgName}
        </small>
          </div>
          <div className="form-group">
          <label htmlFor="inputEmail" >Email</label>
            <input
              type="text"
              placeholder="email"
              name="email"
              id="inputEmail"
              className="form-control"
              value={emailValue}
              onChange={this._updateEmailState.bind(this)}
            />
          <small id="emailHelp" className="form-text text-muted">
          {this.state.errorMsgEmail}
          </small>
          </div>
          <div className="form-group">
          <label htmlFor="inputPhone" >Phone</label>
            <input
              type="text"
              placeholder="phone"
              name="phone"
              id="inputPhone"
              className="form-control"
              value={phoneValue}
              onChange={this._updatePhoneState.bind(this)}
            />
          <small id="phoneHelp" className="form-text text-muted">
          {this.state.errorMsgPhone}
          </small>
          </div>
          <button type="submit" className="btn btn-primary" > {btnValue} </button>
        </form>
      </div>
    );
  }
}
export default inputForm;
