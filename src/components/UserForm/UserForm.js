import React from "react";

const userForm = props => {
  return (
    <form onSubmit={props.formSubmit} >
      <input
        type="text"
        name="name"
        onChange={props.nameUpdateFunc}
        value={props.nameValue}
        placeholder="Name"
      />
      <input
        type="text"
        name="email"
        onChange={props.emailUpdateFunc}
        value={props.emailValue}
        placeholder="Email"
      />
      <input
        type="text"
        name="phone"
        onChange={props.phoneUpdateFunc}
        value={props.phoneValue}
        placeholder="phone"
      />
      <button type="submit"> {props.buttonName} </button>
    </form>
  );
};

export default userForm;
