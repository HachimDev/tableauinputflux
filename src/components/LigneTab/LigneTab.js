import React from 'react';

const ligneTab = (props) => {
    return (
        <tr>
            <td> {props.id} </td>
            <td> {props.name} </td>
            <td> {props.email} </td>
            <td> {props.phone} </td>
            <td> <button className="btn btn-light" onClick={props.deletebtn}>Delete</button> </td>
            <td> <button className="btn btn-light" onClick={props.updatebtn} >Update</button> </td>
        </tr>
    );
};

export default ligneTab;