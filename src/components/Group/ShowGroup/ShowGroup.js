import React, { Component } from "react";
import GroupActions from "../../../actions/GroupActions";
import "./ShowGroup.css";

class ShowGroup extends Component {
  constructor(props) {
    super(props);

    this.state = {
      group: this.props.group,
      toggleSelect: false,
      selectedUserName: ""
    };
  }

  _ConfirmAffectUser = () => {
    if (this.state.selectedUserName !== "") {
      let modifObj = {
        id: this.state.group.id,
        newUserName: this.state.selectedUserName
      };
      GroupActions.editGroup(modifObj);
    } else {
      alert("Incorrect User value");
    }
  };

  _updateSelectedUserName = event => {
    let selectedName = event.target.value;
    this.setState({ selectedUserName: selectedName });
  };

  render() {
    let SelectUser = null;
    if (this.props.users.length !== 0) {
      SelectUser = (
        <div className="selectionBox">
          <select onChange={this._updateSelectedUserName}>
            <option value="--" defaultChecked>--</option>
            {this.props.users.map((user, index) => {
              return (
                <option key={index} value={user.name}>
                  {user.name}
                </option>
              );
            })}
          </select>
        </div>
      );
    }

    return (
      <div className="col">
        <div className="card">
          <div className="card-block">
            <h4 className="card-title"> {this.state.group.name} </h4>
            <p className="card-text"> {this.state.group.description} </p>
          </div>
          <ul className="list-group list-group-flush">
            {this.state.group.users.map((user, index) => {
              return (
                <li key={index} className="list-group-item">
                  {user}
                </li>
              );
            })}
          </ul>
          <button
            onClick={this._ConfirmAffectUser.bind(this)}
            className="btn btn-primary btnAffectUser"
          >
            Affect user
          </button>
          {SelectUser}
        </div>
      </div>
    );
  }
}

export default ShowGroup;
