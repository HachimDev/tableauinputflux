import React, { Component } from "react";
import "./CreateGrp.css";

import GroupActions from "../../../actions/GroupActions";

class CreateGrp extends Component {
  constructor() {
    super();

    this.state = {
      group: {
        name: "",
        description:'Neque porro quisquam est, qui dolorem ipsum quia',
        users:[]
      }
    };
  }

  _getFreshGroup() {
    return {
      name: "",
      description:'Neque porro quisquam est, qui dolorem ipsum quia',
      users:[]
    };
  }

  _updateGroupName = event => {
    let newGrp = {
      name: event.target.value,
      description: 'Neque porro quisquam est, qui dolorem ipsum quia',
      users:[]
    };

    this.setState({
      group: newGrp
    });
  };

  createNewGrpHandler = event => {
    event.preventDefault();
    GroupActions.addNewGroup(this.state.group);
    this.setState({
      group: this._getFreshGroup()
    });
  };

  
  render() {
    return (
      <div className="createGrp">
        <form onSubmit={this.createNewGrpHandler.bind(this)}>
          <div className="form-group">
            <label htmlFor="nameGrp">Group Name</label>
            <input
              onChange={this._updateGroupName}
              value={this.state.group.name}
              className="form-control"
              type="text"
              placeholder="Group Name"
              id="nameGrp"
            />
          </div>

          <button className="btn btn-primary" type="submit">
            Create
          </button>
        </form>
      </div>
    );
  }
}

export default CreateGrp;
