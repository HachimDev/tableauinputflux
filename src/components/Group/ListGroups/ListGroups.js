import React, { Component } from "react";
import CreateGroup from "../CreateGrp/CreateGrp";
import GroupStore from "../../../stores/GroupStore";
import ShowGroup from "../ShowGroup/ShowGroup";
import UserStore from '../../../stores/UserStore';

class ListGroups extends Component {
  constructor() {
    super();

    this.state = {
      groups: GroupStore.getAllGroups(),
      newGrp: false,
      users: UserStore.getAllUsers()
    };

    this._onChange = this._onChange.bind(this);
  }

  _onChange() {
    this.setState({ groups: GroupStore.getAllGroups() });
  }

  componentWillMount() {
    GroupStore.addChangeListener(this._onChange);
  }

  componentWillUnmount() {
    GroupStore.removeChangeListener(this._onChange);
  }

  AddNewGroupHandler = () => {
    this.setState({
      newGrp: !this.state.newGrp
    });
  };

  render() {
    let NewGrpForm = null;

    if (this.state.newGrp) {
      NewGrpForm = (
      <div className="col-2">
        <CreateGroup />
    </div>)
    }

    return (
      <div className="container">
        <div className="row">
            <div className="col-2">
                <button onClick={this.AddNewGroupHandler} className="btn btn-secondary">
                Create Group
                </button>
            </div>
            <div className="col-10" ></div>
        </div>
        <hr />
        <div className="row" >
            {NewGrpForm}
            {this.state.groups.map((grp, index) => {
                return (
                        <ShowGroup key={index} group={grp} users={this.state.users} />
                  );
              })}

        </div>
      </div>
    );
  }
}

export default ListGroups;
