import {EventEmitter} from "events";
import Dispatcher from "../dispatcher/Dispatcher";
import ActionsTypes from "../constants";

const CHANGE = 'CHANGE';
let _groupState = [
    {id:0, name:'MSS', description:'Lorem ipsum dolor sit amet, consectetur adipiscing elit', users:[]},
    {id:1, name:'HOME', description:'Sed ut perspiciatis unde omnis iste natus error sit voluptatem', users:[]}
];

class GroupStore extends EventEmitter {
    constructor(){
        super();

        Dispatcher.register(this._registerToActions.bind(this));
    }

    _registerToActions(action){
        switch(action.type){
            case ActionsTypes.ADD_NEW_GROUP:
                this._addNewGroup(action.payload);
                break;
            case ActionsTypes.EDIT_GROUP:
                this._editGroup(action.payload);
                break;
            default:
            break;
        }
    }

    _editGroup(modifObj){

        _groupState.forEach(element => {
            if(element.id === modifObj.id){
                element.users.push(modifObj.newUserName);
                console.log('new Group : ');
                console.log(element);
                this.emit(CHANGE);
            }
        });
    }

    _addNewGroup(group){
        group.id = _groupState.length;
        _groupState.push(group);
        this.emit(CHANGE);
    }

    getAllGroups(){
        console.log('GETALLGROUPS');
        return _groupState;
    }

    addChangeListener(callback){
        this.on(CHANGE, callback);
    }

    removeChangeListener(callback){
        this.removeListener(CHANGE, callback);
    }
}

export default new GroupStore();