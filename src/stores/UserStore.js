import { EventEmitter } from "events";
import Dispatcher from "../dispatcher/Dispatcher";
import ActionTypes from "../constants";

const CHANGE = "CHANGE";
let _userState = [
  {id:0, name:'Hachim', email:'hachim@example.com', phone:'066666666', pwd:'123456'},
  {id:1, name:'Ahlam', email:'ahlam@example.com', phone:'054544545', pwd:'123456'},
  {id:2, name:'Yassine', email:'yassine@example.com', phone:'065221254', pwd:'123456'},
];
let _userToEdit = "";
let _logged = false;

class UserStore extends EventEmitter {
  constructor() {
    super();

    Dispatcher.register(this._registerToActions.bind(this));
  }

  _registerToActions(action) {

    switch (action.type) {
      case ActionTypes.ADD_NEW_USER:
        this._addNewUser(action.payload);
        break;
      case ActionTypes.DELETE_USER:
        this._deleteUser(action.payload);
        break;
      case ActionTypes.UPDATE_USER:
        this._updateUser(action.payload);
        break;
      case ActionTypes.EDIT_USER:
        this._setUserToEdit(action.payload);
        break;
      case ActionTypes.SIGN_IN:
        console.log('case SIGN IN');
        this._signIn(action.payload);
        break;

      default:
        break;
    }
  }

  _signIn(userToCheck){ //check if the user exist
    function isDefined(el){
      return (el.email === userToCheck.email &&  el.pwd === userToCheck.pwd)
    }
    _logged = (_userState.find(isDefined) !== undefined);
    console.log("_logged : ",_logged);
    this.emit(CHANGE);
  }

  isLogged(){
    console.log('isLogged Call', _logged);
    return _logged;
  }

  _updateUser(updatedUser){

    _userState[updatedUser.id] = updatedUser;
    _userToEdit = "";
    this.emit(CHANGE);
  }


  wantToUpdate(){
    if(_userToEdit !== ""){
      // We want to update a user field
      return true;
    }
    else {
      //We dont want to update a user field
      return false;
    }
  }

  _setUserToEdit(user) {
    _userToEdit = user;
    this.emit(CHANGE);
  }

  getUserToEdit() {
    return _userToEdit;
  }

  getUserToUpdate(user) {
    return user;
  }

  _deleteUser(id) {
    const users = _userState;
    users.splice(id, 1);
    this.emit(CHANGE);
  }

  _addNewUser(user) {
    user.id = _userState.length;
    _userState.push(user);
    this.emit(CHANGE);
  }

  getAllUsers() {
    return _userState;
  }

  addChangeListener(callback) {
    this.on(CHANGE, callback);
  }

  removeChangeListener(callback) {
    this.removeListener(CHANGE, callback);
  }
}

export default new UserStore();
