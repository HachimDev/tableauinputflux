import React, { Component } from "react";
import Tableau from "../../components/Tableau/Tableau";
import AuthForm from "../../components/AuthForm/AuthForm";
import InputForm from "../../components/InputForm/InputForm";
import ListGroup from "../../components/Group/ListGroups/ListGroups";
import UserStore from "../../stores/UserStore";
import {Route, Redirect, NavLink, Switch } from 'react-router-dom';
import "./Page.css";


class Page extends Component {
  constructor(props) {
    super(props);

    this.state = {
      users: UserStore.getAllUsers(),
      userToEdit: {},
      wantToEdit: false
    };
    this._onChange = this._onChange.bind(this);
  }

  _onChange() {
    this.setState({ users: UserStore.getAllUsers() });
    this.setState({ userToEdit: UserStore.getUserToEdit() });
    this.setState({ wantToEdit: UserStore.wantToUpdate() });
  }

  componentWillMount() {
    UserStore.addChangeListener(this._onChange);
  }

  componentWillUnmount() {
    UserStore.removeChangeListener(this._onChange);
  }

  render() {
    return (
      <div className="Page">
      <header>
      <nav >
        <ul>
          <li> <NavLink to="/tableau">Users List</NavLink></li>
          <li> <NavLink to="/new-user" >New User</NavLink> </li>
          <li> <NavLink to="/groups"> Groups </NavLink> </li>
          <li> <NavLink to="/auth"> Disconnect </NavLink> </li>
        </ul>
      </nav>
    </header>
      <Switch>
        <Route path="/tableau" exact render={(routeProps) => (
          <Tableau {...routeProps} users={this.state.users} />
        )} />
        <Route path="/auth" exact component={AuthForm} />

        <Route path="/new-user" exact render={(routeProps) => (
          <InputForm {...routeProps} userToEdit={this.state.userToEdit} wantToEdit={this.state.wantToEdit} />
        )} />
        <Route path="/groups" exact component={ListGroup} />

        <Redirect from="/" to="/auth" />
        <Route render={()=> <h1> 404 - Page Not Found </h1> } />
        {/* <Route component={NotFoundPage} */}
      </Switch>

      </div>
    );
  }
}

export default Page;
