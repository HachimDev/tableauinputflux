import Dispatcher from '../dispatcher/Dispatcher';
import ActionsTypes from '../constants';


class UserActions {

    addNewUser(user) {
        Dispatcher.dispatch({
            type: ActionsTypes.ADD_NEW_USER,
            payload: user
        });
    }

    deleteUser(id){
        Dispatcher.dispatch({
            type: ActionsTypes.DELETE_USER,
            payload: id
        });
    }

    updateUser(userToUpdate){
        Dispatcher.dispatch({
            type:ActionsTypes.UPDATE_USER,
            payload: userToUpdate
        });
    }

    editUser(userToEdit){
        Dispatcher.dispatch({
            type: ActionsTypes.EDIT_USER,
            payload: userToEdit
        });
    }

    signIn(user){
        Dispatcher.dispatch({
            type: ActionsTypes.SIGN_IN,
            payload: user
        })
    }
}

export default new UserActions();