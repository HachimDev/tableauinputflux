import Dispatcher from '../dispatcher/Dispatcher';
import ActionsTypes from '../constants';

class GroupActions {

    addNewGroup(group){
        Dispatcher.dispatch({
            type: ActionsTypes.ADD_NEW_GROUP,
            payload: group 
        });
    }

    //modifObj is an object {id: ..., newUserName: ...}
    editGroup(modifObj){ //When adding a user to a group
        Dispatcher.dispatch({
            type: ActionsTypes.EDIT_GROUP,
            payload: modifObj
        })
    }
}

export default new GroupActions();